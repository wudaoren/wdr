package web

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

const GC_TIME = 60 //单位:秒

type Sequence struct {
	gced      bool
	wait      int64
	requested *sync.Map
}

//防止重复提交的中间件
func SequenceHandler(wait int64, onrepeat func(*gin.Context)) gin.HandlerFunc {
	var seq = new(Sequence)
	seq.requested = new(sync.Map)
	seq.wait = wait
	seq.gc()
	return func(c *gin.Context) {
		if c.Request.Method == "GET" {
			return
		}
		// 把request的内容读取出来
		var bodyBytes []byte
		if c.Request.Body != nil {
			bodyBytes, _ = ioutil.ReadAll(c.Request.Body)
		}
		sha := sha1.New()
		keyBytes := []byte(fmt.Sprint(c.Request.URL, c.Request.Method, c.GetHeader("Authorization"), c.ClientIP()))
		keyBytes = append(keyBytes, bodyBytes...)
		sha.Write(keyBytes)
		key := hex.EncodeToString(sha.Sum([]byte("")))
		// 把刚刚读出来的再写进去
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		now := time.Now().Unix()
		if tm, ok := seq.requested.Load(key); ok {
			if seq.isLimited(now, tm) {
				onrepeat(c)
				c.Abort()
				return
			}
		}
		seq.requested.Store(key, now)
	}
}

//是否受限制：true限制，false不限制
func (this *Sequence) isLimited(now int64, tm interface{}) bool {
	if visitedTime, ok := tm.(int64); ok && now-visitedTime > this.wait {
		return false
	}
	return true
}

//当请求的时间超过gc时间则清除该请求
func (this *Sequence) gc() {
	if this.gced {
		return
	}
	this.gced = true
	go func() {
		defer recover()
		for {
			time.Sleep(time.Second * time.Duration(GC_TIME))
			now := time.Now().Unix()
			this.requested.Range(func(k, tm interface{}) bool {
				if !this.isLimited(now, tm) {
					this.requested.Delete(k)
				}
				return true
			})
		}
	}()
}
