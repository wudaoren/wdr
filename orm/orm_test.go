package orm

import (
	"errors"
	"fmt"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"xorm.io/core"
)

type User struct {
	Id      int    `xorm:"int(11) autoincr pk" model:""`
	Name    string `xorm:"varchar(32)" put:"user"`
	Age     int64  `xorm:"int(11)" put:"user"`
	Version int    `xorm:"version" put:"sss"`
	Model   `xorm:"-"`
}

func NewUser() *User {
	u := new(User)
	u.InitModel(u)
	return u
}

func (this *User) TableName() string {
	return "User"
}
func (this *User) PrimaryKey() interface{} {
	return this.Id
}

//乐观锁测试
func initdata() {
	eg, err := xorm.NewEngine("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", "root", "", "127.0.0.1:3306", "test"))
	if err != nil {
		fmt.Println(err)
		return
	}
	eg.Charset("utf8")
	eg.ShowSQL(true)
	eg.Sync2(NewUser())
	eg.SetMapper(new(core.SameMapper))
	InitEngine(eg)
}

func TestTrans(t *testing.T) {
	initdata()
	e := TransWarp(func(eg *Trans) error {
		user := NewUser()
		user.ExtendEngine(eg)
		user.Name = "唐僧"
		user.Age = 30
		if err := user.Insert(); err != nil {
			return err
		}
		fmt.Printf("eg:%p\n", eg.GetEngine())
		fmt.Printf("user:%p\n", user.GetEngine())
		userb := NewUser()
		userb.ExtendEngine(eg)
		err := userb.Transaction(func(sx *Session) error {
			userb.Name = "孙悟空"
			userb.Age = 500
			if err := userb.Insert(); err != nil {
				return err
			}
			fmt.Printf("userb:%p\n", userb.GetEngine())
			//return errors.New("孙悟空错误")
			userc := NewUser()
			userc.ExtendEngine(userb)
			err := userc.Transaction(func(sx *Session) error {
				userc.Name = "猪八戒"
				userc.Age = 800
				if err := userc.Insert(); err != nil {
					return err
				}
				fmt.Printf("userc:%p\n", userc.GetEngine())

				//return nil
				return errors.New("猪八戒错误")
			})
			return err
		})
		return err
	})
	fmt.Println("err is:", e, errors.New(""))
}
