package orm

import (
	"github.com/go-xorm/xorm"
)

//模型接口
type ModelInterface interface {
	PrimaryKey() interface{} //主键id
	TableName() string       //表名
}

//模块接口
type ModuleInterface interface {
	Install() error
}

//引擎接口
type Enginer interface {
	SetEngine(*xorm.Engine)
	GetEngine() *xorm.Engine
	Session() *xorm.Session
	Transaction(func(sx *xorm.Session) error) error
}
