package orm

import (
	"errors"
	"reflect"
	"runtime/debug"

	"gitee.com/wudaoren/wdr/errs"

	"github.com/go-xorm/xorm"
)

//引擎结构体
type Engine struct {
	parent     Enginer
	sx         *xorm.Session
	eg         *xorm.Engine
	beginTrans bool
}

//设置引擎
func (this *Engine) SetEngine(eg *xorm.Engine) {
	if this.parent != nil {
		this.parent.SetEngine(eg)
		return
	}
	this.eg = eg
}

//获取引擎
func (this *Engine) GetEngine() *xorm.Engine {
	if this.parent != nil {
		return this.parent.GetEngine()
	}
	if this.eg == nil {
		if engine == nil {
			panic("请先初始化数据库引擎。")
		}
		this.eg = engine
	}
	return this.eg
}

//获取session
func (this *Engine) Session() *xorm.Session {
	if this.parent != nil {
		return this.parent.Session()
	}
	if this.sx == nil {
		this.sx = this.GetEngine().NewSession()
	}
	return this.sx
}

//继承引擎
func (this *Engine) ExtendEngine(parent Enginer) {
	this.parent = parent
}

//表
func (this *Engine) DB(tableNameOrBean interface{}) *xorm.Session {
	return this.Session().Table(tableNameOrBean)
}

//事务
func (this *Engine) Transaction(call func(sx *xorm.Session) error) (err error) {
	if this.parent != nil {
		return this.parent.Transaction(call)
	}
	sx := this.Session()
	if this.beginTrans {
		return call(sx)
	}
	this.beginTrans = true
	if err = sx.Begin(); err != nil {
		return
	}
	defer func() {
		if e := recover(); e != nil {
			err = errs.Fatal(e, "\n", string(debug.Stack()))
		}
		if err != nil {
			sx.Rollback()
		} else {
			err = sx.Commit()
		}
	}()
	err = call(this.sx)
	return
}

//不使用继承方式直接创建一个模型
func (this *Engine) Model(obj ModelInterface) *Model {
	var model = new(Model)
	model.ExtendEngine(this)
	return model.InitModel(obj)
}

//当更新的数据不是1条时返回错误
func (this *Engine) IsOneChange(changes int64, e error) error {
	if e != nil {
		return errs.Fatal(e.Error())
	} else if changes != 1 {
		return errors.New("存储失败")
	}
	return nil
}

//查询异常判断
func (this *Engine) IsGetOK(ok bool, e error) error {
	if e != nil {
		return errs.Fatal(e.Error())
	} else if !ok {
		return errors.New("查询错误")
	}
	return nil
}

//分页查询
//listPtr	= 查询列表指针
//page	= 页码
//limit	= 每页查询数量
func (this *Engine) FindPage(session *xorm.Session, listPtr interface{}, page, limit int) (total int64, err error) {
	if page <= 0 {
		page = 1
	}
	if limit == 0 {
		limit = 20
	}
	total, err = session.Clone().Select("count(*)").Count()
	if err != nil {
		return 0, err
	}
	start := (page - 1) * limit
	err = session.Limit(limit, start).Find(listPtr)
	if err != nil {
		return 0, errs.Fatal(err.Error())
	}
	return total, err
}

//通过反射获取切片结构体id切片
func (this *Engine) GetFieldList(list interface{}, idFieldName string) ([]interface{}, error) {
	v := reflect.ValueOf(list).Elem()
	if v.Kind() != reflect.Slice {
		return nil, errors.New("必须传入一个slice类型数据")
	}
	fieldMaps := make(map[interface{}]bool) //id集合
	for i := 0; i < v.Len(); i++ {
		ptr := v.Index(i)
		if reflect.ValueOf(ptr).Kind() != reflect.Struct {
			return nil, errors.New("slice的元素必须是struct")
		}
		if ptr.Kind() != reflect.Ptr {
			ptr = ptr.Addr()
		}
		ptr = ptr.Elem()
		val := ptr.FieldByName(idFieldName).Interface()
		fieldMaps[val] = true
	}
	filedSlice := make([]interface{}, 0)
	for field, _ := range fieldMaps {
		filedSlice = append(filedSlice, field)
	}
	return filedSlice, nil
}
