package mongo

import (
	"reflect"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Model struct {
	Engine                //继承引擎基类
	obj    ModelInterface //model对象
	coll   *mgo.Collection
	id     reflect.Value
}

//初始化数据模型，obj为指针
func (this *Model) InitModel(obj ModelInterface) *Model {
	this.obj = obj
	val := reflect.ValueOf(obj).Elem()
	this.id = val.FieldByName("Id")
	return this
}

//集合对象
func (this *Model) Collection() *mgo.Collection {
	if this.coll == nil {
		this.coll = this.C(this.obj.CollectionName())
	}
	return this.coll
}

//获取主键id
func (this *Model) getId() bson.ObjectId {
	return this.id.Interface().(bson.ObjectId)
}

//添加
func (this *Model) Insert() error {
	this.id.Set(reflect.ValueOf(bson.NewObjectId()))
	return this.Collection().Insert(this.obj)
}

//修改
func (this *Model) Update() error {
	return this.Collection().UpdateId(this.getId(), this.obj)
}

//删除
func (this *Model) Delete() error {
	println("删除的事：", this.getId().Hex())
	return this.Collection().RemoveId(this.getId())
}

//查询(默认id查询）
func (this *Model) Get() error {
	return this.GetById(this.getId().Hex())
}

//指定id查询
func (this *Model) GetById(id string) error {
	obj := this.obj
	defer func() {
		this.InitModel(obj)
	}()
	return this.Collection().FindId(bson.ObjectIdHex(id)).One(this.obj)
}

//根据条件查询
func (this *Model) GetBy(b M) error {
	obj := this.obj
	defer func() {
		this.InitModel(obj)
	}()
	return this.Collection().Find(b).One(this.obj)
}
