package mongo

import (
	"strings"

	"github.com/globalsign/mgo"
)

//mongo数据库引擎
var engine *mgo.Session

//初始化引擎
func InitEngine(eg *mgo.Session) {
	engine = eg
}

//
type Engine struct {
}

//
func (this *Engine) DB(name string) *mgo.Database {
	return engine.DB(name)
}

//例如 "IM.Message"
func (this *Engine) C(dbAndcollection string) *mgo.Collection {
	table := strings.Split(dbAndcollection, ".")
	if len(table) != 2 {
		panic("db.collections错误:" + dbAndcollection)
	}
	return engine.DB(table[0]).C(table[1])
}

//分页查询
//参数：query=查询参数，page=页，limit=每页数量,listPtr=查询列表
//返回：total=总数据量，err=cuow

func (this *Engine) FindPage(query *mgo.Query, page, limit int, listPtr interface{}) (total int, err error) {
	if limit == 0 {
		limit = 20
	}
	total, err = query.Count()
	if err != nil {
		return
	}
	if page <= 0 {
		page = 1
	}
	start := (page - 1) * limit
	err = query.Limit(limit).Skip(start).All(listPtr)
	return
}
