package mongo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/globalsign/mgo"
)

type User struct {
	Id       ObjectId  `bson:"_id"`
	UserName string    `bson:"UserName"`
	Age      int64     `bson:"Age"`
	Time     time.Time `bson:"Time"`
	Model    `bson:"-"`
}

func NewUser() *User {
	user := new(User)
	user.InitModel(user)
	return user
}

//
func (this *User) CollectionName() string {
	return "Test.User"
}

//加载mongodb
func TestDefault(t *testing.T) {
	dialInfo := &mgo.DialInfo{
		Addrs:     []string{"127.0.0.1:27017"},
		Direct:    false,
		Timeout:   time.Second * 5,
		PoolLimit: 1000, // Session.SetPoolLimit
		Username:  "",
		Password:  "",
	}
	//创建一个维护套接字池的session
	engine, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		t.Fatal(err.Error())
	}
	engine.SetMode(mgo.Monotonic, true)
	InitEngine(engine)
	//保存
	user := NewUser()
	user.UserName = "wang"
	user.Age = 30
	user.Time = time.Now()
	if err := user.Insert(); err != nil {
		t.Fatal("1添加失败：", err.Error())
	} else {
		t.Log("1添加成功")
	}
	//修改
	user.UserName = "王海涛"
	if err := user.Update(); err != nil {
		t.Fatal("2修改失败：", err.Error())
	} else {
		t.Log("2修改成功")
	}
	//多查询
	eg := new(Engine)
	list := make([]*User, 0)
	if err := eg.C(user.CollectionName()).Find(M{}).All(&list); err != nil {
		t.Fatal("3多查询失败：", err.Error())
	} else {
		t.Log("3多查询成功：", len(list))
	}
	//单查询
	nuser := NewUser()
	t.Logf("地址：%p", nuser)
	if err := nuser.GetById(list[0].Id.Hex()); err != nil {
		t.Fatal("4单查询失败：", err.Error())
	} else {
		t.Log("4单查询成功：", nuser.UserName, nuser.Id, nuser.obj)
	}
	t.Logf("地址：%p", nuser)
	//删除
	if err := nuser.Delete(); err != nil {
		t.Fatal("5单删除失败：", err.Error())
	} else {
		t.Log("5单删除成功")
	}
	q := user.Collection().Find(M{})
	nlist := make([]*User, 0)
	if total, err := user.FindPage(q, 1, 5, &nlist); err != nil {
		t.Fatal("6分页查询失败：", err.Error())
	} else {
		t.Log("6分页查询成功：", total, len(nlist))
	}
	var str bytes.Buffer
	data, _ := json.Marshal(list)
	_ = json.Indent(&str, data, "", "    ")
	fmt.Println("formated: ", str.String())
	//
}
