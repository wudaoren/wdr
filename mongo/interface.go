package mongo

import (
	"github.com/globalsign/mgo/bson"
)

//模型接口
type ModelInterface interface {
	CollectionName() string //集合名
}

//map
type M map[string]interface{}

//
type ObjectId = bson.ObjectId
